# phab-gitlab-bridge

This repo contains a tool to bridge Gitlab issues with Phabricator tasks.

The main goal is to be able to handle issues on Gitlab which keep a task associated
in Phabricator to make it easier to manage and to have a place to exchange non-public
data.

There are two main uses of this tool:
- Import Phabricator tasks into Gitlab issues, which basically creates a Gitlab issue
per open Phabricator task associated with a project.

- Sync Phabricator tasks from Gitlab issues, which consist in scanning all open Phabricator
task associated with a Gitlab issue and update title, description, status, priority and tags.