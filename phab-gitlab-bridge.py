#!/usr/bin/python3

import argparse
import logging
import gitlab
import phabricator
import re
from config import config

PHABRICATOR_LINK_TAG = 'Phabricator link'

GITLAB_ISSUE_IID_TAG = 'Gitlab issue iid'
GITLAB_ISSUE_LINK_TAG = 'Gitlab issue link'

MANAGEMENT_DATA_TAG = 'Management data'
MANAGEMENT_DATA_COMMENT = 'This section is for management only, it should be the last one in the description.'

LAVA_TAG = 'apertis:lava:suite'

PHAB_STATUS_IN_BACKLOG = ['unconfirmed', 'confirmed']
PHAB_STATUS_IN_PROGRESS = ['inprogress']
PHAB_STATUS_UPSTREAM = ['waitupstream']
PHAB_STATUS_IN_REVIEW = ['needinfo', 'fixproposed', 'fixsubmitted']
PHAB_STATUS_OPEN = PHAB_STATUS_IN_BACKLOG + PHAB_STATUS_IN_PROGRESS + PHAB_STATUS_UPSTREAM + PHAB_STATUS_IN_REVIEW
PHAB_STATUS_CLOSE = ['resolved', 'verified', 'closed', 'wontfix', 'invalid', 'duplicate']
PHAB_STATUS_ALL = PHAB_STATUS_OPEN + PHAB_STATUS_CLOSE

PHAB_PRIORITY_HIGH = [100, 90, 80]
PHAB_PRIORITY_TRIAGE = [70]
PHAB_PRIORITY_NORMAL = [60, 50, 40]
PHAB_PRIORITY_HIGH_TRIAGE_NORMAL = PHAB_PRIORITY_HIGH + PHAB_PRIORITY_TRIAGE + PHAB_PRIORITY_NORMAL
PHAB_PRIORITY_LOW = [30, 25, 0]
PHAB_PRIORITY_ALL = PHAB_PRIORITY_HIGH_TRIAGE_NORMAL + PHAB_PRIORITY_LOW

PHAB_STATUSES = {
    'backlog': PHAB_STATUS_IN_BACKLOG,
    'inprogress': PHAB_STATUS_IN_PROGRESS,
    'inreview': PHAB_STATUS_IN_REVIEW,
    'open': PHAB_STATUS_OPEN,
    'close': PHAB_STATUS_CLOSE,
    'all': PHAB_STATUS_ALL,
}

PHAB_PRIORITIES = {
    'high': PHAB_PRIORITY_HIGH,
    'triage': PHAB_PRIORITY_TRIAGE,
    'normal': PHAB_PRIORITY_NORMAL,
    'high-triage-normal': PHAB_PRIORITY_HIGH_TRIAGE_NORMAL,
    'low': PHAB_PRIORITY_LOW,
    'all': PHAB_PRIORITY_ALL
}

GITLAB_STATUSES = {
    'open': 'opened',
    'close': 'closed',
    'all': None
}

def get_releases():
    releases = []
    for r in range(config['first-release-year'], config['last-release-year']):
        for d in ['dev0', 'dev1', 'dev2', 'dev3', 'pre', '']:
            releases.append('v' + str(r) + d)
    return releases

class PhabGitlabBridge():
    def __init__(self, phab_token = '', gitlab_token = None):
        self.phid_to_name_map = {}
        self.user_map_invert = None

        phab_host = config['phabricator-url'] + '/api/'
        phab_token = phab_token or config['phabricator-token']

        self.phab = phabricator.Phabricator(host = phab_host, token = phab_token)
        try:
            self.phab.update_interfaces()
            logging.info('Will talk to Phabricator as %s' % self.phab.user.whoami().userName)
        except Exception as e:
            logging.error('Phab API connection failed: %s', e)

        if config['gitlab-instance']:
            self.gitlab = gitlab.Gitlab.from_config()
        else:
            if not gitlab_token and config['gitlab-token']:
                gitlab_token = config['gitlab-token']
            self.gitlab = gitlab.Gitlab(config['gitlab-url'], gitlab_token)

    def phab_name_to_phid(self, name):
        # the name cannot contain colons
        name = name.replace(':', '_')
        r = self.phab.phid.lookup(names=[name])
        if isinstance(r.response, dict) and name in r:
            return r[name]['phid']
        else:
            return None

    def phab_phid_to_name(self, phid):
        # the name cannot contain colons
        if phid in self.phid_to_name_map:
            return self.phid_to_name_map[phid]
        r = self.phab.phid.query(phids=[phid])
        if phid in r:
            self.phid_to_name_map[phid] = r[phid]['name'].lower()
            return self.phid_to_name_map[phid]
        else:
            return None

    def phab_fetch_statuses(self):
        r = self.phab.maniphest.status.search()
        statuses = []
        for s in r['data']:
            statuses.append(s['value'].lower())

        return statuses

    def phab_fetch_priorities(self):
        r = self.phab.maniphest.priority.search()
        priorities = []
        for p in r['data']:
            priorities.append(p['name'].lower())

        return priorities

    def phab_fetch_tasks_pages(self, constraints, attachments):
        after = None
        data = []
        while True:
            r = self.phab.maniphest.search(
                constraints = constraints,
                attachments = attachments,
                after = after,
                order = ['-id'])
            data += r['data']
            after = r['cursor']['after']
            if not after:
                break

        return data

    def phab_fetch_tasks(self, statuses = PHAB_STATUS_OPEN, priorities = None):
        logging.info(f'Fetching tasks')
        constraints = {}
        constraints['projects'] = config['phabricator-projects']
        if statuses: constraints['statuses'] = statuses
        if priorities: constraints['priorities'] = priorities

        tasks = self.phab_fetch_tasks_pages(constraints=constraints, attachments={'projects': 'true'})

        return tasks

    def phab_filter_tasks(self, tasks, task_ids):
        tasks_new = []
        for t in tasks:
            if t['id'] in task_ids:
                tasks_new.append(t)
        return tasks_new

    def phab_print_tasks(self, tasks, verbose = 1):
        for t in tasks:
            if verbose == 1:
                print(f"{t['id']} {t['fields']['name']}")
            else:
                print(t)

    def phab_apply_transactions(self, task_id, transactions):
        if 'projects.add' in transactions:
            transactions['projects.add'] = [phid for phid in [self.phab_name_to_phid('#' + slug) for slug in transactions['projects.add']] if phid]
        if 'projects.remove' in transactions:
            transactions['projects.remove'] = [phid for phid in [self.phab_name_to_phid('#' + slug) for slug in transactions['projects.remove']] if phid]
        if 'space' in transactions:
            transactions['space'] = self.phab_name_to_phid(transactions['space'])
        tr = [{'type': t, 'value': v} for t, v in transactions.items()]
        if not tr:
            logging.debug(f'Empty transaction for task {task_id}')
            return
        logging.info(f'Updating task {task_id}')
        try:
            r = self.phab.maniphest.edit(objectIdentifier=task_id, transactions=tr)
            return r['object']['id']
        except phabricator.APIError as e:
            logging.error('Phabricator API request failed for task %d: %s', task_id, e)
            print(tr)

    def phab_create_task(self, title, description, projects):
        transactions = {}
        transactions['title'] = title
        transactions['description'] = description
        transactions['projects.add'] = projects
        transactions['space'] = config['phabricator-space']
        task_id = self.phab_apply_transactions(None, transactions)

        return task_id

    def phab_update_task(self, id, status, priority):
        transactions = {}
        if status: transactions['status'] = status
        if priority: transactions['priority'] = priority
        task_id = self.phab_apply_transactions(id, transactions)

    def phab_add_gitlab_data(self, task, issue_iid):
        description = task['fields']['description']['raw']
        description += '\n\n'
        description += MANAGEMENT_DATA_TAG + '\n'
        description += '=' * len(MANAGEMENT_DATA_TAG) + '\n'
        description += MANAGEMENT_DATA_COMMENT + '\n\n'
        description += GITLAB_ISSUE_IID_TAG + ': ' + str(issue_iid) + '\n\n'
        gitlab_issue_link = config['gitlab-url'] + config['gitlab-project'] + '/-/issues/'
        description += GITLAB_ISSUE_LINK_TAG + ': ' + gitlab_issue_link + str(issue_iid)
        transactions = {}
        transactions['description'] = description

        self.phab_apply_transactions(task['id'], transactions)

    def phab_map_user_from_gitlab(self, gitlab_user):
        return config['user-map'].get(gitlab_user, gitlab_user)

    def gitlab_fetch_issues(self, state = 'opened', labels = []):
        logging.info(f'Fetching issues')
        project = self.gitlab.projects.get(config['gitlab-project'])
        issues = project.issues.list(state=state, labels=labels, all=True, sort='asc')

        return issues

    def gitlab_filter_issues(self, issues, issues_iids):
        issues_new = []
        for i in issues:
            if i.iid in issues_iids:
                issues_new.append(i)
        return issues_new

    def gitlab_fetch_issue(self, issue_iid):
        project = self.gitlab.projects.get(config['gitlab-project'])
        issue = project.issues.get(issue_iid)

        return issue

    def gitlab_filter_release(self, issues, releases):
        if releases == []:
            return issues

        release_labels_check = set(['release:' + r for r in releases])

        issues_filtered = []
        for i in issues:
            release_labels = set([l for l in i.labels if l.startswith('release')])
            if len(release_labels) == 0:
                release_labels = set(['release:none'])
            if release_labels.intersection(release_labels_check):
                issues_filtered.append(i)

        return issues_filtered

    def gitlab_print_issues(self, issues, verbose = 1):
        base_url = config['gitlab-url'] + config['gitlab-project'] + '/-/issues/'
        for i in issues:
            if verbose == 1:
                print(f"{i.iid} {i.title}")
            elif verbose == 2:
                print(f"[{i.iid}]({base_url}{i.iid}) {i.title}")
            elif verbose == 3:
                print(f"- [Issue #{i.iid}]({base_url}{i.iid}) {i.title}")
            else:
                print(i)

    def gitlab_print_issues_priority(self, issues, verbose = 1):
        priorities = [
            ['High', 'priority:high'],
            ['Needs triage', 'priority:needs triage'],
            ['Normal', 'priority:normal'],
            ['Low', 'priority:low']
        ]
        for p in priorities:
            print(p[0])
            issues_filtered = [x for x in issues if p[1] in x.labels]
            self.gitlab_print_issues(issues_filtered, verbose)
            print()

    def gitlab_delete_issues(self, issues):
        logging.info(f'Deleting issues')
        project = self.gitlab.projects.get(config['gitlab-project'])
        for i in issues:
            project.issues.delete(i.iid)

    def gitlab_create_issue(self, title, description = '', labels = None):
        logging.info(f'Creating issue {title}')
        project = self.gitlab.projects.get(config['gitlab-project'])
        issue = project.issues.create({'title': title,
                               'description': description})

        if labels:
            issue.labels = labels
            issue.save()

        return issue

    def gitlab_create_issues_from_phab(self, tasks, skip_phab_check_update = False):
        for t in tasks:
            #Skip creating gitlab issue is there is already an issue
            if not skip_phab_check_update and t['fields']['description']['raw'].find(GITLAB_ISSUE_IID_TAG) != -1:
                continue

            title = t['fields']['name']
            description = t['fields']['description']['raw']
            description = description.replace('\n[apertis', '\n- [apertis')
            description = description.replace('\n* https', '\n- https')
            description = re.sub(r'== (.*)\n', r'\1\n==========\n', description)
            description += '\n\n'
            if description.find(MANAGEMENT_DATA_TAG) == -1:
                description += MANAGEMENT_DATA_TAG + '\n'
                description += '=' * len(MANAGEMENT_DATA_TAG) + '\n'
                description += MANAGEMENT_DATA_COMMENT + '\n\n'
            if 'custom.apertis:lava:suite' in t['fields'] and t['fields']['custom.apertis:lava:suite']:
                description += LAVA_TAG + ': ' + t['fields']['custom.apertis:lava:suite'] + '\n\n'
            description += PHABRICATOR_LINK_TAG + ': ' + config['phabricator-url'] + '/T' + str(t['id'])

            priority = t['fields']['priority']['name'].lower()
            status = t['fields']['status']['value'].lower()
            projects = t['attachments']['projects']['projectPHIDs']
            projects = [self.phab_phid_to_name(p) for p in projects]
            releases = [p for p in projects if re.search('v\d\d\d\d', p)]
            areas = [p.lower() for p in projects if p.lower() in config['areas']]

            labels = []
            labels.append('priority:' + priority)
            labels.append('status:' + status)
            labels += ['release:' + r for r in releases]
            labels += ['area:' + a for a in areas]

            issue = self.gitlab_create_issue(title, description, labels)

            if t['fields']['ownerPHID']:
                owner = self.phab_phid_to_name(t['fields']['ownerPHID'])
                assignee = self.gitlab_map_user_from_phab(owner)
                assignee_id = self.gitlab_get_user_id(assignee)
                if assignee_id != None:
                    issue.assignee_ids = [assignee_id]
                    issue.save()
                else:
                    logging.warning(f'Unable to map Phabricator user {owner} to Gitlab')

            if not skip_phab_check_update:
                self.phab_add_gitlab_data(t, issue.iid)

    def phab_check_task_no_gitlab(self, tasks, skip_phab_check_update = False):
        for t in tasks:
            if t['fields']['description']['raw'].find(GITLAB_ISSUE_IID_TAG) != -1:
                continue

            id = t['id']
            name = t['fields']['name']
            t_projects = [self.phab_phid_to_name(x) for x in t['attachments']['projects']['projectPHIDs']]
            project_highlight = set(t_projects).intersection(set(config['phabricator-projects-highlight']))

            print(f"T{id} {name} ({' '.join(project_highlight)})")

    def phab_create_tasks_from_gitlab(self, issues):
        logging.info(f'Creating Phabricator tasks based on Gitlab issues')
        gitlab_issue_link = config['gitlab-url'] + config['gitlab-project'] + '/-/issues/'

        for i in issues:
            if ('managed' in i.labels or config['all-issues-managed']) and \
                    i.description.find(PHABRICATOR_LINK_TAG) == -1:
                logging.info(f'Creating task from issue {i.iid}')
                description =  i.description + '\n\n'
                if description.find(MANAGEMENT_DATA_TAG) == -1:
                    description += MANAGEMENT_DATA_TAG + '\n'
                    description += '=' * len(MANAGEMENT_DATA_TAG) + '\n'
                    description += MANAGEMENT_DATA_COMMENT + '\n'
                description += GITLAB_ISSUE_IID_TAG + ': ' + str(i.iid) + '\n'
                description += GITLAB_ISSUE_LINK_TAG + ': ' + gitlab_issue_link + str(i.iid)

                task_id = self.phab_create_task(i.title, description, config['phabricator-projects'])

                r = re.search(r'apertis:lava:suite: (.*)', i.description)
                if r:
                    transactions = {'custom.apertis:lava:suite': r[1]}
                    self.phab_apply_transactions(task_id, transactions)

                description =  i.description
                description += '\n\n'
                if description.find(MANAGEMENT_DATA_TAG) == -1:
                    description += MANAGEMENT_DATA_TAG + '\n'
                    description += '=' * len(MANAGEMENT_DATA_TAG) + '\n'
                    description += MANAGEMENT_DATA_COMMENT + '\n\n'
                description += PHABRICATOR_LINK_TAG + ': ' + config['phabricator-url'] + '/T' + str(task_id)
                i.description = description
                i.save()

    def gitlab_check_open_phab(self, issues):
        logging.info(f'Checking Gitlab issues vs Phabricator status')
        for i in issues:
            r = re.search(PHABRICATOR_LINK_TAG + ': (.*)', i.description)
            if r:
                task_id = r[1].split('/')[-1][1:]
                task = self.phab.maniphest.info(task_id = int(task_id))
                if not task['status'] in PHAB_STATUS_OPEN:
                    logging.warning(f"Task {task_id} is opened in Gitlab but closed with {task['status']} on Phab")
                    statuses = [elem for elem in i.labels if elem.startswith('status:')]
                    if len(statuses):
                        status = statuses[0][len('status:'):]
                    else:
                        status = PHAB_STATUS_IN_BACKLOG[0]
                    transactions = {'status' : status}
                    self.phab_apply_transactions(task_id, transactions)

    def gitlab_check_labels(self, issues):
        logging.info(f'Checking Gitlab labels')
        labels_default = {
            'opened': {'status': 'status:unconfirmed', 'priority': 'priority:needs triage'},
            'closed': {'status': 'status:resolved', 'priority': 'priority:needs triage'} }
        labels_opened = ['status:' + x for x in PHAB_STATUS_OPEN]
        labels_closed = ['status:' + x for x in PHAB_STATUS_CLOSE]
        labels_valid = {'opened': labels_opened, 'closed': labels_closed }

        for i in issues:
            labels_check = {'status': [], 'priority': []}

            labels_remove = []
            for l in i.labels:
                for k in labels_valid.keys():
                    if i.state == k and l.startswith('status:') and l not in labels_valid[k]:
                        logging.warning(f'Issue {i.iid} has label {l} but its state is {k}')
                        labels_remove.append(l)

            if len(labels_remove):
                for l in labels_remove:
                    i.labels.remove(l)
                i.save()

            for l in i.labels:
                for k in labels_check.keys():
                    if l.startswith(k):
                        labels_check[k].append(l)
            for k in labels_check.keys():
                if len(labels_check[k]) > 1:
                    logging.warning(f'Issue {i.iid} has multiple {k} labels')
                    for j in range(1, len(labels_check[k])):
                        i.labels.remove(labels_check[k][j])
                        i.save()
                if len(labels_check[k]) == 0:
                    logging.warning(f'Issue {i.iid} has no {k} label')
                    i.labels.append(labels_default[i.state][k])
                    i.save()

    def gitlab_get_user_id(self, username):
        users = self.gitlab.users.list(username=username)
        if not users:
            return None

        return users[0].id

    def gitlab_map_user_from_phab(self, phab_user):
        if self.user_map_invert is None:
            self.user_map_invert = {}
            for k,v in config['user-map'].items():
                self.user_map_invert[v] = k

        return self.user_map_invert.get(phab_user, phab_user)

    def phab_description_from_gitlab(self, task, issue):
        description_issue = issue.description
        description_task = task['fields']['description']['raw']

        pos = description_issue.find(MANAGEMENT_DATA_TAG)
        description_issue_main = description_issue[:pos]

        pos = description_task.find(MANAGEMENT_DATA_TAG)
        description_task_main = description_task[:pos]
        description_task_management = description_task[pos:]

        if description_issue_main == description_task_main:
            return None

        description_task = description_issue_main + description_task_management

        return description_task

    def phab_properties_from_gitlab(self, issue):
        properties = {}
        properties['title'] = issue.title

        labels_to_properties = {'priority:': 'priority', 'status:': 'status', 'area:': 'projects', 'release:': 'projects'}

        for l in issue.labels:
            for lt in labels_to_properties:
                if l.startswith(lt):
                    t = labels_to_properties[lt]
                    v = l[len(lt):]
                    if t == 'projects':
                        if t not in properties:
                            properties[t] = [v]
                        else:
                            properties[t].append(v)
                    else:
                        properties[t] = v

        return properties

    def phab_transaction_from_properties(self, task, properties):
        transactions = {}

        if task['fields']['name'] != properties['title']:
            transactions['title'] = properties['title']

        fields = {'status': 'value', 'priority': 'name'}

        for f in fields:
            if f in properties and properties[f] != task['fields'][f][fields[f]].lower():
                value = properties[f]
                # Phabricator does not accept space on priority keywords
                if f == 'priority' and value.find(' ') != 1:
                    value = value.replace(' ', '')
                transactions[f] = value

        projects_task = task['attachments']['projects']['projectPHIDs']
        projects_task = [self.phab_phid_to_name(p) for p in projects_task]
        projects_task = set(projects_task).intersection(get_releases() + config['areas'])

        projects_issue = properties['projects'] if 'projects' in properties else []

        projects_add = list(set(projects_issue).difference(projects_task))
        projects_remove = list(set(projects_task).difference(projects_issue))

        if len(projects_add):
            transactions['projects.add'] = projects_add

        if len(projects_remove):
            transactions['projects.remove'] = projects_remove

        return transactions

    def phab_update_task_from_gitlab(self, task, issue):
        logging.info(f"Updating task {task['id']} from issue {issue.iid}")
        properties = self.phab_properties_from_gitlab(issue)
        transactions = self.phab_transaction_from_properties(task, properties)

        description = self.phab_description_from_gitlab(task, issue)
        if description:
            transactions['description'] = description

        phab_owner = None
        desired_phab_owner = None
        if task['fields']['ownerPHID']:
            phab_owner = self.phab_phid_to_name(task['fields']['ownerPHID'])
        if issue.assignees:
            desired_phab_owner = self.phab_map_user_from_gitlab(issue.assignees[0]['username'])
        if desired_phab_owner != phab_owner:
            if desired_phab_owner:
                desired_phab_owner_id = self.phab_name_to_phid('@'+desired_phab_owner)
                if desired_phab_owner_id:
                    transactions['owner'] = desired_phab_owner_id
                else:
                    logging.warning(f"Unable to map Gitlab user {issue.assignees[0]['username']} to Phabricator")
            else:
                transactions['owner'] = None

        if config['block-task'] and task['fields']['policy']['edit'] != 'administrators':
            transactions['edit'] = 'administrators'

        if not transactions:
            logging.info(f"Empty transaction for task {task['id']}")
            return

        self.phab_apply_transactions(task['id'], transactions)

    def phab_update_tasks_from_gitlab(self, tasks):
        logging.info(f'Updating Phabricator tasks based on Gitlab issues')
        for t in tasks:
            description = t['fields']['description']['raw']
            r = re.search(GITLAB_ISSUE_IID_TAG + ': (.*)', description)
            if r:
                issue_iid = r[1]
                issue = self.gitlab_fetch_issue(issue_iid)
                self.gitlab_check_labels([issue])
                issue = self.gitlab_fetch_issue(issue_iid)
                self.phab_update_task_from_gitlab(t, issue)

    def gitlab_fetch_labels(self):
        logging.info(f'Fetching labels')
        project = self.gitlab.projects.get(config['gitlab-project'])
        labels = project.labels.list(all=True)

        return labels

    def gitlab_get_label(self, labels, label_name):
        for l in labels:
            if l.name == label_name:
                return l
        return None

    def gitlab_print_labels(self, labels):
        print('labels:')
        for l in labels:
            print(f'  - name: {l.name}\n    color: {l.color}')

    def gitlab_update_labels(self):
        logging.info(f'Updating labels')
        project = self.gitlab.projects.get(config['gitlab-project'])
        labels = self.gitlab_fetch_labels()

        statuses_labels = ['status:' + x for x in self.phab_fetch_statuses()]
        priorities_labels = ['priority:' + x for x in self.phab_fetch_priorities()]
        release_labels = ['release:' + x for x in get_releases()]
        area_labels = ['area:' + x for x in config['areas']]
        labels_config = []
        labels_config.append([statuses_labels, '#cc338b'])
        labels_config.append([priorities_labels, '#6699cc'])
        labels_config.append([release_labels, '#00b140'])
        labels_config.append([area_labels, '#808080'])
        labels_config.append([config['extra-labels'], '#ff0000'])

        for lc in labels_config:
            color = lc[1]
            for ln in lc[0]:
                label = self.gitlab_get_label(labels, ln)
                if label:
                    label.color = lc[1]
                    label.save()
                else:
                    project.labels.create({'name': ln, 'color': color})

if __name__ == '__main__':
    logging.basicConfig(level = logging.INFO)

    action_choices = [
        'phab-print-tasks',
        'phab-create-task-from-gitlab',
        'phab-update-task-from-gitlab',
        'phab-check-task-no-gitlab',
        'gitlab-print-issues',
        'gitlab-print-issues-priority',
        'gitlab-create-issues-from-phab',
        'gitlab-check-labels',
        'gitlab-check-open-phab',
        'gitlab-print-labels',
        'gitlab-update-labels',
        'sync'
    ]

    available_actions = ' '.join(action_choices)

    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--config', default='config.yaml', help='configuration file')
    parser.add_argument('-d', '--dev', action='store_true', help='development mode')
    parser.add_argument('-g', '--gitlab-token', default='', help='Gitlab token')
    parser.add_argument('-k', '--skip-check', action='store_true', help='skip checks')
    parser.add_argument('-l', '--label',default='', help='label to filter')
    parser.add_argument('-p', '--priorities', default='all', help='filter by maniphest priorities')
    parser.add_argument('-r', '--release', dest='releases', default=[], action='append', help='releases to filter (use "none" for no release)')
    parser.add_argument('-s', '--statuses', default='open', help='filter by maniphest statuses')
    parser.add_argument('-t', '--phab-token', default='', help='Phabricator token')
    parser.add_argument('-v', '--verbose', type=int, default=1, help='verbose')
    parser.add_argument('--filter-task', dest='filter_tasks', type=int, action='append', default=[], help='tasks to filter')
    parser.add_argument('--filter-issue', dest='filter_issues', type=int, action='append', default=[], help='issues to filter')
    parser.add_argument('action', choices=action_choices, help='action to be executed, available actions: ' + available_actions, metavar='action')

    args = parser.parse_args()

    config.load_config(args)

    phab_priorities = PHAB_PRIORITIES[args.priorities]
    phab_statuses = PHAB_STATUSES[args.statuses]
    gitlab_state = GITLAB_STATUSES[args.statuses]
    bridge = PhabGitlabBridge(args.phab_token, args.gitlab_token)
    label = [args.label]

    if args.action == 'phab-print-tasks':
        tasks = bridge.phab_fetch_tasks(phab_statuses, phab_priorities)
        if args.filter_tasks:
            tasks = bridge.phab_filter_tasks(tasks, args.filter_tasks)
        if args.dev:
            tasks = bridge.phab_filter_tasks(tasks, config['filter-tasks'])
        bridge.phab_print_tasks(tasks, args.verbose)
    elif args.action == 'phab-create-task-from-gitlab':
        issues = bridge.gitlab_fetch_issues(gitlab_state)
        bridge.phab_create_tasks_from_gitlab(issues)
    elif args.action == 'phab-update-task-from-gitlab':
        tasks = bridge.phab_fetch_tasks(phab_statuses, phab_priorities)
        if args.filter_tasks:
            tasks = bridge.phab_filter_tasks(tasks, args.filter_tasks)
        if args.dev:
            tasks = bridge.phab_filter_tasks(tasks, config['filter-tasks'])
        bridge.phab_update_tasks_from_gitlab(tasks)
    elif args.action == 'gitlab-print-issues':
        issues = bridge.gitlab_fetch_issues(gitlab_state, label)
        issues = bridge.gitlab_filter_release(issues, args.releases)
        if args.filter_issues:
            tasks = bridge.gitlab_filter_issues(issues, args.filter_issues)
        if args.dev:
            issues = bridge.gitlab_filter_issues(issues, config['filter-issues'])
        bridge.gitlab_print_issues(issues, args.verbose)
    elif args.action == 'gitlab-print-issues-priority':
        issues = bridge.gitlab_fetch_issues(gitlab_state, label)
        issues = bridge.gitlab_filter_release(issues, args.releases)
        if args.filter_issues:
            tasks = bridge.gitlab_filter_issues(issues, args.filter_issues)
        if args.dev:
            issues = bridge.gitlab_filter_issues(issues, config['filter-issues'])
        bridge.gitlab_print_issues_priority(issues, args.verbose)
    elif args.action == 'gitlab-create-issues-from-phab':
        tasks = bridge.phab_fetch_tasks(phab_statuses, phab_priorities)
        if args.filter_tasks:
            tasks = bridge.phab_filter_tasks(tasks, args.filter_tasks)
        if args.dev:
            tasks = bridge.phab_filter_tasks(tasks, config['filter-tasks'])
        bridge.gitlab_create_issues_from_phab(tasks, args.skip_check)
    elif args.action == 'phab-check-task-no-gitlab':
        tasks = bridge.phab_fetch_tasks(phab_statuses, phab_priorities)
        if args.filter_tasks:
            tasks = bridge.phab_filter_tasks(tasks, args.filter_tasks)
        if args.dev:
            tasks = bridge.phab_filter_tasks(tasks, config['filter-tasks'])
        bridge.phab_check_task_no_gitlab(tasks, args.skip_check)
    elif args.action == 'gitlab-check-open-phab':
        issues = bridge.gitlab_fetch_issues(gitlab_state)
        if args.filter_issues:
            tasks = bridge.gitlab_filter_issues(issues, args.filter_issues)
        if args.dev:
            issues = bridge.gitlab_filter_issues(issues, config['filter-issues'])
        bridge.gitlab_check_open_phab(issues)
    elif args.action == 'gitlab-check-labels':
        issues = bridge.gitlab_fetch_issues(gitlab_state)
        if args.filter_issues:
            tasks = bridge.gitlab_filter_issues(issues, args.filter_issues)
        if args.dev:
            issues = bridge.gitlab_filter_issues(issues, config['filter-issues'])
        bridge.gitlab_check_labels(issues)
    elif args.action == 'gitlab-print-labels':
        labels = bridge.gitlab_fetch_labels()
        bridge.gitlab_print_labels(labels)
    elif args.action == 'gitlab-update-labels':
        bridge.gitlab_update_labels()
    elif args.action == 'sync':
        issues = bridge.gitlab_fetch_issues(gitlab_state)
        if args.filter_issues:
            tasks = bridge.gitlab_filter_issues(issues, args.filter_issues)
        if args.dev:
            issues = bridge.gitlab_filter_issues(issues, config['filter-issues'])
        bridge.gitlab_check_labels(issues)
        bridge.gitlab_check_open_phab(issues)
        bridge.phab_create_tasks_from_gitlab(issues)
        tasks = bridge.phab_fetch_tasks(phab_statuses, phab_priorities)
        if args.filter_tasks:
            tasks = bridge.phab_filter_tasks(tasks, args.filter_tasks)
        if args.dev:
            tasks = bridge.phab_filter_tasks(tasks, config['filter-tasks'])
        bridge.phab_update_tasks_from_gitlab(tasks)
    else:
        print('Invalid action')